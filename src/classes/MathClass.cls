public with sharing class MathClass {
	
	public static Integer sum(Integer a, Integer b) {
		return a + b;
	}
}