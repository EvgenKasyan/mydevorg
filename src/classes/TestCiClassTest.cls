@isTest
public with sharing class TestCiClassTest {
	
	static testmethod void testSum() {
		TestCiClass test_ci = new TestCiClass();
		Integer result = test_ci.sum(1,2);
		System.assertEquals(3, result);
	}
}