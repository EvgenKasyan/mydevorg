/**
 * Created by Tom on 27/01/2018.
 */

public with sharing class AuthorsRepo {

    public void testSelect() {
        List<Author_Book_Junction__mdt> authorsBookJunctions = [SELECT Book__r.Title__c, Author__r.First_Name__c,
                Author__r.Last_Name__c  FROM Author_Book_Junction__mdt];
        System.debug(authorsBookJunctions);
    }
}