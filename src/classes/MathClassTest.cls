@isTest
public with sharing class MathClassTest {
	
	@isTest
	static void sumTestSuccess() {
		Integer result = MathClass.sum(1,2);
		System.assertEquals(3, result);
	}
}