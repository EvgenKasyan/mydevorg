<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>The Old Man and the Sea</label>
    <protected>false</protected>
    <values>
        <field>Title__c</field>
        <value xsi:type="xsd:string">The Old Man and the Sea</value>
    </values>
</CustomMetadata>
