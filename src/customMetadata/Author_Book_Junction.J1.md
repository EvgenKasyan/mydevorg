<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>J1</label>
    <protected>false</protected>
    <values>
        <field>Author__c</field>
        <value xsi:type="xsd:string">Ernest_Hemingway</value>
    </values>
    <values>
        <field>Book__c</field>
        <value xsi:type="xsd:string">The_Old_Man_and_the_Sea</value>
    </values>
</CustomMetadata>
